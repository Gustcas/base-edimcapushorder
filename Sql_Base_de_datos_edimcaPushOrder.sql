
CREATE ROLE edimca WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	REPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'edimca.01';
COMMENT ON ROLE edimca IS 'Administrador de datos';


CREATE DATABASE "edimcaPushOrder"
    WITH 
    OWNER = edimca
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
COMMENT ON DATABASE "edimcaPushOrder"
    IS 'edimcaPushOrder';


create table LOGIN (
   IDUSUARIO            SERIAL        ,
   NOMBREUSUARIO        VARCHAR(500)         null,
   APELLIDOUSUARIO      VARCHAR(500)         null,
   USUARIO              VARCHAR(500)         null,
   PASSWORD             VARCHAR(500)         null,
   ESTADO               CHAR(1)              null,
   constraint PK_LOGIN primary key (IDUSUARIO)
);


create unique index LOGIN_PK on LOGIN (
IDUSUARIO
);

create table ORDENCABECERA (
   IDORDEN                SERIAL        ,
   FECHACREACIONORDEN   VARCHAR(500)         null,
   NOMBRECLIENTE        VARCHAR(500)         null,
   CEDULACLIENTE        VARCHAR(13)          null,
   DIRECCIONCLIENTE     VARCHAR(500)         null,
   ESTADO               CHAR(1)              null,
   constraint PK_ORDENCABECERA primary key (IDORDEN)
);
create unique index ORDENCABECERA_PK on ORDENCABECERA (
IDORDEN
);

create table PRODUCTO (
   IDPRODUCTO            SERIAL        ,
   NOMBREPRODUCTO       VARCHAR(500)         null,
   PRECIOVENTA          DECIMAL(5,2)         null,
   STOCK                INT4                 null,
   ESTADO               CHAR(1)              null,
   constraint PK_PRODUCTO primary key (IDPRODUCTO)
);
create unique index PRODUCTO_PK on PRODUCTO (
IDPRODUCTO
);

create table ORDENDETALLE (
   IDORDENDETALLE         SERIAL        ,
   IDPRODUCTO           INT4                 null,
   IDORDEN              INT4                 null,
   CANTIDAD             INT4                 null,
   IVA                  DECIMAL(5,2)         null,
   TOTAL                DECIMAL(5,2)         null,
   ESTADO               CHAR(1)              null,
   constraint PK_ORDENDETALLE primary key (IDORDENDETALLE)
);
create unique index ORDENDETALLE_PK on ORDENDETALLE (
IDORDENDETALLE
);

INSERT INTO public.login(
nombreusuario, apellidousuario, usuario, password, estado)
VALUES ( 'admin', 'admin', 'admin', 'admin', 'A');


